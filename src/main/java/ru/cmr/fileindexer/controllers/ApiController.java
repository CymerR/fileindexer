package ru.cmr.fileindexer.controllers;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.json.JsonGeneratorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.bind.annotation.*;
import ru.cmr.fileindexer.model.FileList;
import ru.cmr.fileindexer.model.QueryKeeper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "api")
public class ApiController {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @GetMapping
    public String test() {
        logWithDate("Get request.");
        return "nothing be interesting";
    }

    @PostMapping(produces = "application/json")
    public List<File> querySearch(@RequestBody QueryKeeper query) {
        logger.info(query.toString());
        log("Post request");
        FileIndexer indexer = new FileIndexer(query);
        return indexer.find();
    }

    private void logWithDate(String text) {
        Date time = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yy MM/dd hh:mm:ss");
        log(formatter.format(time) + ": " + text);
    }
    private void log(String text) {
        logger.info(text);
    }
}
