package ru.cmr.fileindexer.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cmr.fileindexer.model.QueryKeeper;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileIndexer {

    /**
     *  Логгирование
     */
    private final Logger logger = LoggerFactory.getLogger(FileIndexer.class);

    /**
     *  Структура данных содержащая ту строчку, по которой фильтруются файлы
     */
    private final QueryKeeper query;


    public FileIndexer(QueryKeeper query) {
        this.query = query;
    }

    /**
     *  Функция, которая производит поиск файлов по определеной строке,
     *  передаваемой в конструктор класса
     * @return List<File> - список найденных файлов
     */
    public List<File> find() {
        return files().stream()
                .filter(this::filterFile)
                .collect(Collectors.toList());
    }


    /**
     *  Функция - предикат, который используется для фильтрации файлов по разным признакам
     * @param f - Файл, который фильтруется
     * @return true - если файл подходит по параметрам(Названию и контенту)
     */
    private boolean filterFile(File f) {
        String qry = query.getQuery();
        boolean byName = f.getName().contains(qry);
        boolean byContent = false;
        try (Stream<String> lines = Files.lines(f.toPath())){
            byContent = lines.anyMatch(l -> l.contains(qry));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (byName | byContent);
    }
    //TODO: Property value configuration
    private String directory = "files";

    public List<File> files() {
        File[] files = new File(directory).listFiles();
        List<File> res = new ArrayList<>();
        if (files == null) return res;
        res.addAll(Arrays.asList(files));
        return res;
    }
}
