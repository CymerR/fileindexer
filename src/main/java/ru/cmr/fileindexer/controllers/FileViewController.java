package ru.cmr.fileindexer.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;
import java.util.stream.Collectors;

@Controller
public class FileViewController {
    Logger logger = LoggerFactory.getLogger(FileViewController.class);

    @ResponseBody
    @GetMapping("files/{filename}")
    public String index(@PathVariable("filename") String filename) {
        logger.info("Received filename:| " + filename);
        if (filename == null || filename.isEmpty()) {
            logger.info("Empty or null filename");
            return "No file provided";
        }
        try {
            return new BufferedReader(new FileReader("files/" + filename))
                    .lines()
                    .peek(logger::debug)
                    .collect(Collectors.joining());
        } catch (IOException e) {
            logger.error("Some error happened", e);
            return "Sorry, something <strong>bad</strong> have happened!";
        }

    }
}
