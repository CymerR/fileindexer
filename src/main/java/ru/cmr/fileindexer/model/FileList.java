package ru.cmr.fileindexer.model;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FileList {

    private List<File> files;
    private Date creationTime;
    private String query;

    public FileList(List<File> files, String qry) {
        this.files = files;
        this.query = qry;
        creationTime = Calendar.getInstance().getTime();
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
