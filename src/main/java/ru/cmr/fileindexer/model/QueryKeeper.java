package ru.cmr.fileindexer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

@JsonFormat
public class QueryKeeper {

    private String query;
    private Date creationTime;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }


    @Override
    public String toString() {
        String formatted = new SimpleDateFormat().format(creationTime);
        return String.format("{ query: %s, creationTime: %s}" , query, formatted);
    }
}
