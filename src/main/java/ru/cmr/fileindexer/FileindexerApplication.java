package ru.cmr.fileindexer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileindexerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileindexerApplication.class, args);
	}

}
