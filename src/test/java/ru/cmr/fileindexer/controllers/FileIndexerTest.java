package ru.cmr.fileindexer.controllers;

import org.junit.jupiter.api.Test;
import ru.cmr.fileindexer.model.QueryKeeper;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class FileIndexerTest {

    @Test
    void files_1() {
        Object[] files = new FileIndexer(new QueryKeeper()).files().stream()
                .map(File::getName).toArray();
        String[] expected = new String[] {"another.html", "title.html"};
        assertArrayEquals(expected, files);
    }

    @Test
    void find_by_title() {
        QueryKeeper qry = new QueryKeeper();
        qry.setQuery("another");
        Object[] files = new FileIndexer(qry).find().stream()
                .map(File::getName)
                .toArray();
        String[] expected = new String[]{"another.html"};

       assertArrayEquals(expected, files);
    }


    @Test
    void find_by_content_1() {
        QueryKeeper qry = new QueryKeeper();
        qry.setQuery("kontent_1");
        Object[] files = new FileIndexer(qry).find()
                .stream()
                .map(File::getName)
                .toArray();
        String[] expected = new String[] {"another.html"};
        assertArrayEquals(expected, files);
    }

    @Test
    void string_contains_method_studying() {
        String l1 = "Hell";
        String l2 = "<div>Hello</div>";
        assertTrue(l2.contains(l1));
    }
}