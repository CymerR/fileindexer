

Vue.use(VueResource)

let item_template = `
  <div class="list_item"><a :href="filepath" target="_blank">{{ shortName }}</a></div>
`
Vue.component('list-item',{
  props: [ 'file' ],
  data: function () {
    return {
      filename: this.file,
    }
  },
  computed: {
    shortName() {
      let arr = this.filename.split("/");
      console.log(arr);
      return arr[arr.length-1];
    },
    filepath() {
      return "files/" + this.shortName
    }
  },
  template: item_template
})



new Vue({
 el: "#app",
 data: function() {
   return {
     label: '',
     obj: null,
     files: [],
     isQueryProcessed: false
   }
 },
 mounted: function() {
   // this.$http.get("https://jsonplaceholder.typicode.com/posts/1")
   // .then(res => {
   //   this.obj = res.body
   // })
 },
 methods: {
   postQuery() {
     console.log("query method enter");
     this.isQueryProcessed = true;
     let data = {
       query: this.label,
       creationTime: Date.now()
     }
     this.$http.post("/api", data)
     .then(res => {
       this.files = res.body
       console.log(this.files);
     })
   }
 }
})
